extern "C" {
	#include <wiringPi.h>
}

#include <iostream>
#include <cstdlib>
#include <unistd.h>

int pins[4] = {0,1,29,3};

int stepCounts = 0;

int pattern[8][4] = {
		{1, 0, 0, 1},
		{1, 0, 0, 0},
		{1, 1, 0, 0},
		{0, 1, 0, 0},
		{0, 1, 1, 0},
		{0, 0, 1, 0},
		{0, 0, 1, 1},
		{0, 0, 0, 1}
	};



void step() {
	for(int p = 0; p < 4; p++) {
		int pin = pins[p];
		if (pattern[stepCounts][p] != 0) {
			digitalWrite(pin, true);
		} else {
			digitalWrite(pin, false);
		}
	}
	stepCounts++;

	if(stepCounts >= 8) {
		stepCounts = 0;
	} else if(stepCounts < 0) {
		stepCounts = 2;
	}
}

void step(int steps) {
	int completedSteps = 0;
	for(int s = 0; s < steps; s++){
		step();
		usleep(500);
	}
}

void cleanup() {
	for(int p = 0; p < 4; p++){
		digitalWrite(pins[p], false);
	}
}

int main(void) {
	wiringPiSetup();
       
	std::atexit(cleanup);

	for(int i = 0; i < 4; i++){ 
		pinMode(pins[i], OUTPUT);
	}

	step(8000);


        return 0;
}

